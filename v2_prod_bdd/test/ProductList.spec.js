// test/ProductList.spec.js
const assert = require('assert');
const puppeteer = require('puppeteer');

describe( 'Feature: Products List', () => {
  describe('Given: Empty List', () => {
    it('When: Search One Item, Then: Not Found');
  });
  describe('Given: Non Empty List', () => {
    it('When: Search One Item, Then: Find One Item', async () => {
        const browser = await puppeteer.launch({
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox'
            ]
        });    
        const page = await browser.newPage();
        await page.goto('http://192.168.16.2:3000/', {waitUntil: 'domcontentloaded'}); //, {waitUntil: 'networkidle2'});
        let textResponse = await page.evaluate(() => {
          let elemCheck = document.querySelector('.product_name');
          return elemCheck.innerHTML;
        });
        assert( textResponse === 'Banana', "[OK] Content Found" );
        browser.close();
    }).timeout(0);
 
    it('When: Search One Item, Then: Find One Item And name And Price');
  });
});

