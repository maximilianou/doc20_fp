import React from 'react';

const ProductList = () => {
  return(
    <ul>
      <li><span class='product_name'>Banana</span><span class='product_price'>40</span></li>
      <li>Apple 50</li>
      <li>Strawberry 150</li>
      <li>Watermelon 30</li>
      <li>Pineapple 25</li>
      <li>Radish 30</li>
      <li>Corn 100</li>
      <li>Tomato 60</li>
    </ul>
    );
}

export default ProductList;
