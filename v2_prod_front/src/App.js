import React from 'react';
import ProductList from './ProductList';
const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <ProductList /> 
      </header>
    </div>
  );
}

export default App;
