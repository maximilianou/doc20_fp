import React, { createContext } from 'react';
const DispatchContext = createContext(null);
export default DispatchContext;
