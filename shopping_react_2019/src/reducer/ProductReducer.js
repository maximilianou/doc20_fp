import React from 'react';
import uuid from 'uuid/v4';
export const initalProducts = [
    {
      id: uuid(),
      name: 'Radish',
      desc: 'vegetable',
      img: 'img/Radish_256.svg'
    },
    {
      id: uuid(),
      name: 'Pineapple',
      desc: 'fruit',
      img: 'img/Pineapple_256.svg'
    },
    {
      id: uuid(),
      name: 'Tomato',
      desc: 'vegetable',
      img: 'img/Tomato_256.svg'
    },
    {
      id: uuid(),
      name: 'Watermelon',
      desc: 'fruit',
      img: 'img/Watermelon_256.svg'
    }
  ];

export const filterReducer = (state, action) => {
    switch( action.type){
      case 'SHOW_ALL':
        return 'ALL';
      case 'SHOW_FRUITS':
        return 'FRUITS';
      case 'SHOW_VEGETABLES':
        return 'VEGETABLES';
      default:
        return state;
    }
};  

const productReducer = (state, action) => {
  switch( action.type ){
      case 'ADD_PRODUCT':
        return [ ...state, { id: uuid(), name: action.payload.name, desc: action.payload.desc } ];
      default:
        return state;
  }
};
export default productReducer;