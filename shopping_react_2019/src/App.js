import React, { useReducer } from 'react';
import Products from './Products';
import Cart from './Cart';
import ProductFrom from './ProductFrom';
import FilterButtons from './FilterButtons';
import DispatchContext from './store/DispatchContext';
import  productReducer, {filterReducer, initalProducts } from './reducer/ProductReducer'

const App = () => {
  const [filter, dispatchFilter] = useReducer(filterReducer, 'ALL');
  const [products, dispatchProducts] = useReducer( productReducer, initalProducts );
  const dispatch = action => 
        [dispatchProducts, dispatchFilter].forEach( fn => fn(action)); 
  return (
    <DispatchContext.Provider value={dispatch}>
      <header><h1>Shopping Cart React 2019 ( like redux  ) </h1></header>
      <div class='container'>
        <FilterButtons />
        <Products />
        <ProductFrom />
        <Cart />
      </div>
    </DispatchContext.Provider>
  );
}
export default App;
