import React, { useContext } from 'react';
import DispatchContext from './store/DispatchContext'

const FilterButtons = () => {
    const dispatch = useContext(DispatchContext);
    const handleShowAll = () => {
      dispatch({ type: 'SHOW_ALL' });
    };
  
    const handleShowFruits = () => {
      dispatch({ type: 'SHOW_FRUITS' });
    };
  
    const handleShowVegetables = () => {
      dispatch({ type: 'SHOW_VEGETABLES' });
    };
  
    return (
      <section>
        <button type="button" onClick={handleShowAll}>
          Show All
        </button>
        <button type="button" onClick={handleShowFruits}>
          Show Fruits
        </button>
        <button type="button" onClick={handleShowVegetables}>
          Show Vegetables
        </button>
      </section>
    );
  };
export default FilterButtons;