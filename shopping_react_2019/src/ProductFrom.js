import React, { useState, useContext } from 'react';
import DispatchContext from './store/DispatchContext';

const ProductFrom = () => {
  const dispatch = useContext(DispatchContext);
  const [product, setProduct] = useState('');
  const handleSubmit = event => {
    if( product ){
      dispatch({type: 'ADD_PRODUCT', product});
    }
    setProduct('');
    event.preventDefault();
  };
  const handleChange = event => setProduct(event.target.value);
  return (
    <section class='form_input'>
      <form onSubmit={handleSubmit} >
        <label>Name
          <input type='text' value={product} onChange={handleChange} />
        </label>
        <label>Desc
          <textarea onChange={handleChange} >{product}</textarea>
        </label>
        <button type="submit">Add Product</button>
      </form>
    </section>
)};
export default ProductFrom;